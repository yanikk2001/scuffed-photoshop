Scuffed Photoshop Ver 1.0
// Supported image formats: PBM(Ascii), PGM(Ascii), PPM(Ascii) \\

Commands:
1. To open a specific image: open <filepath>
Example: open C:\Users\img.pbm, if there is white space in the name use quotes: open "C:\Desktop\new img.ppm"

2. To create a new file with background filled with one color: new <width> <height> <color>
Note: File format will be determined depending on the value the user inputs as color 
Example: new 1280 720 1 (This will create a 1280x720 pbm file filled with 1, ec white)
Example: new 1080 1080 #ff (This will create a pgm file filled with pixel values of 250, eg white)
Example: new 50 50 #00ff00 (This will create a ppm file filled with pixel values of (0, 250, 0), eg green)

3. To close the current file: close
Note: If it detects that the file is not saved it will prompt the user to save it or type close again to close it without saving

4. To save a current file: save
Note: If the current file has no save directory (ex: it was created with "new" command) it will prompt the user to use "saveas"

5. To save the current file to a new directory: saveas <directory>
Example: saveas E:\img.pbm or if the directory name contains white spaces: saveas "E:\hello world.ppm"

6. To exit the program: exit
Note: If the current file is not properly closed it will prompt the user to do so

7. To crop the image: crop <leftcorner Y value> <leftcorner X value> <rightcorner Y value> <rightcorner X value>
Example: if we have a 100x100 image and crop it using: crop 50 50 100 100 , it will produce an image with 50x50 dimensions

8. To scale-up the image: scale <new width> <new height>
Example: if we have a 375x100 image and scale it using: scale 1280 720 , it will produce an image with 1280x720 dimensions 

9. To apply a dithering effect to an image: dither <name of dithering algorithm>
Available algorithms:
- FLOYD-STEINBERG
- J-J-N
- STUCKI
- ATKINSON
- BURKES
- SIERRA
- TWO-ROW-SIERRA
- SIERRA-LITE
Note: capitalization doesn`t matter