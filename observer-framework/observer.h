#pragma once
#include <string>

class Subject; //Pre-defined class to prevent recursive definition 

//Interface class
class IObserver
{
public:
  virtual ~IObserver(){};
  virtual void Update(const std::string &message_from_subject) = 0;
};

class Observer : public IObserver
{
  std::string message_from_subject;
  Subject* subject = nullptr;

public:
  /*
    Constructor observer, attaches observer to subject
    @param subject sbj to be attached to   
  */
  Observer(Subject &subject);

  Observer();
  
  /*
    Updates the field: message_from_subject
    @info called by Subject`s Notify()
  */
  virtual void Update(const std::string &message_from_subject_input) override;

  /*
    Removes observer from sbj`s list
    @info only works if this->subject != nullptr
  */
  void RemoveMeFromTheList();
  const std::string& getMessageObs() const;
};