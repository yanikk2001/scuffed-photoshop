CC = g++

CFLAGS = -c -Wall -std=c++17

SOURCEDIR=.\source
FRAMEWORKDIR = .\observer-framework
FORMATDIR = .\file-formats
DITHERDIR = .\dither
TESTDIR = .\tests

all: scuffPh

scuffPh: main.o observer.o subject.o view.o controller.o imageEditor.o PPM.o PBM.o PGM.o InterfaceFormats.o dither.o
	$(CC) main.o observer.o subject.o view.o controller.o imageEditor.o PPM.o PBM.o PGM.o InterfaceFormats.o dither.o -o scuffPh 

test-vc: view-controller_test.o observer.o subject.o view.o controller.o
	$(CC) view-controller_test.o observer.o subject.o view.o controller.o -o test-vc

main.o: $(addprefix $(SOURCEDIR)\, main.cpp)
	$(CC) $(CFLAGS) $(addprefix $(SOURCEDIR)\, main.cpp)

observer.o: $(addprefix $(FRAMEWORKDIR)\, observer.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FRAMEWORKDIR)\, observer.cpp)

subject.o: $(addprefix $(FRAMEWORKDIR)\, subject.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FRAMEWORKDIR)\, subject.cpp)

view.o: $(addprefix $(SOURCEDIR)\, view.cpp)
	$(CC) $(CFLAGS) $(addprefix $(SOURCEDIR)\, view.cpp)

controller.o: $(addprefix $(SOURCEDIR)\, controller.cpp)
	$(CC) $(CFLAGS) $(addprefix $(SOURCEDIR)\, controller.cpp)

imageEditor.o: $(addprefix $(SOURCEDIR)\, imageEditor.cpp) 
	$(CC) $(CFLAGS) $(addprefix $(SOURCEDIR)\, imageEditor.cpp)

InterfaceFormats.o: $(addprefix $(FORMATDIR)\, InterfaceFormats.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FORMATDIR)\, InterfaceFormats.cpp) 

PPM.o: $(addprefix $(FORMATDIR)\, PPM.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FORMATDIR)\, PPM.cpp)

PBM.o: $(addprefix $(FORMATDIR)\, PBM.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FORMATDIR)\, PBM.cpp)

PGM.o: $(addprefix $(FORMATDIR)\, PGM.cpp)
	$(CC) $(CFLAGS) $(addprefix $(FORMATDIR)\, PGM.cpp)
	
dither.o: $(addprefix $(DITHERDIR)\, dither.cpp)
	$(CC) $(CFLAGS) $(addprefix $(DITHERDIR)\, dither.cpp)

view-controller_test.o: $(addprefix $(TESTDIR)\, view-controller_test.cpp)
	$(CC) $(CFLAGS) $(addprefix $(TESTDIR)\, view-controller_test.cpp)

clean:
	rm -rf *o scuffPh


