#pragma once
#include <fstream>
#include <variant>
#include <algorithm>
#include <cmath>

#include "..\file-formats\PPM.h"
#include "..\file-formats\PBM.h"
#include "..\file-formats\PGM.h"

#include "..\dither\dither.h"

enum fileformat
{
    P1,
    P2,
    P3
};

class imageEditor 
{
    std::variant<PPM, PBM, PGM> file; //Variant supports PBM, PGM, PPM
    bool isLoaded = false;
    std::string filepath; //Current filepath
    fileformat fformat;

    /*
        Depending on the file format calls the format`s open method
        @exception invalid_argument if it fails to open file
    */
    void loadImg(const std::string& format);

    /*
        Used by save and saveAs. Depending on the file format it calls the format`s save method
        @exception std::exception if it fails to open file or there is an already loaded image 
    */
    void saveHelper(const std::string& saveLocation);

    /*
        Depending on the file format creates new file in memory only filled one color
        @param color this value is used to determine file format 
    */
    void newFileHelper(const std::string& width, const std::string& height, std::string color);

    /*
        Removes .ppm or .pgm extension and puts .pbm. Used when saving after dithering
    */
    void updateFilepath();

    template <typename T, typename FORMAT>
    std::vector<T> cropHelper(recpoint& leftcorner, recpoint& rightcorner, FORMAT& format);

    /*
        Nearest neighbor algorithm
        @param w1 original width
        @param h1 original height
        @param w2 new width
        @param h2 new height
    */
    template <typename T>
    std::vector<T> resizePixels(std::vector<T> pixels, int w1, int h1, int w2, int h2);
    
public:
    //Constuctor used when controller calls for OPEN file
    imageEditor(const std::string& filepath_input);

    //Constuctor used when controller calls for NEW file
    imageEditor(const std::string& width, const std::string& height, const std::string& color);

    ~imageEditor();

    void save();
    void saveAs(const std::string& saveLocation);
    bool close();
    void crop(const std::string& cor1, const std::string& cor2, const std::string& cor3, const std::string& cor4);
    void dither(const std::string& ditherName);
    void scale(const std::string& newWidth, const std::string& newHeight);

    const fileformat getFileFormat() const;
    const bool getLoadState() const;
};