#include "controller.h"

void Controller::Update(const std::string &message_from_subject_input) 
{
    Observer::Update(message_from_subject_input);

    std::vector<std::string> arguments = Controller::proccessInput(message_from_subject_input); // Arr with input

    CommandType command;
    toUpper(arguments[0]);
    command = stringToCommand(arguments[0]);
    
    switch (command)
    {
        case OPEN: //----------------------------------------------------------------------------------------------
        {
            if(arguments.size() < 2)
            {
                setAndNotify("Invalid use. Correct: open filepath"); 
                break;
            }
            try
            {
                if(this->imgEdit != nullptr)
                {
                    setAndNotify("There is already an open file. Close it first before opening another one!");
                    break;
                }
                this->imgEdit = new imageEditor(arguments[1]);
            }
            catch(const std::bad_alloc& e)
            {
                setAndNotify("Failed to allocate memory for imageEditor");
                break;
            }
            catch(const std::invalid_argument& e)
            {
                deleteImgEditor();
                setAndNotify(e.what());
                break;
            }
            catch(const std::runtime_error& e)
            {
                deleteImgEditor();
                setAndNotify(e.what());
                break;
            }
            
            setAndNotify("File is open");
            break;
        }

        case CLOSE: //----------------------------------------------------------------------------------------------
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before closing");
                break;
            }
            try
            {
                if(!this->imgEdit->close())
                    break;
            }
            catch(const std::invalid_argument& e)
            {
                setAndNotify(e.what());
                break;
            }
            
            deleteImgEditor();
            setAndNotify("File is closed");
            break;
        }

        case SAVEAS: //----------------------------------------------------------------------------------------------
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before savingas");
                break;
            }
            if(arguments.size() < 2)
            {
                setAndNotify("Invalid use. Correct: saveas filepath"); 
                break;
            }
            try
            {
                checkIfCorrectFormatUsed(arguments[1]);
                this->imgEdit->saveAs(arguments[1]);
            }
            catch(const std::invalid_argument& e)
            {
                setAndNotify(e.what());
                break;
            }
            catch(const std::runtime_error& e)
            {
                setAndNotify(e.what());
                break;
            }
            
            setAndNotify("File saved");
            break;
        }

        case NEW: //----------------------------------------------------------------------------------------------
        {
            if(arguments.size() < 4)
            {
                setAndNotify("Invalid use. Correct: new width height #color"); 
                break;
            }
            try
            {
                if(this->imgEdit != nullptr)
                {
                    setAndNotify("There is already an open file. Close it first before opening another one!");
                    break;
                }
                this->imgEdit = new imageEditor(arguments[1], arguments[2], arguments[3]);
            }
            catch(const std::bad_alloc& e)
            {
                setAndNotify("Failed to allocate memory for imageEditor");
                break;
            }
            catch(const std::out_of_range& e)
            {
                deleteImgEditor();
                setAndNotify(e.what());
                break;
            }
            
            setAndNotify("Created new file");
            break;
        }

        case SAVE: //----------------------------------------------------------------------------------------------
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before saving");
                break;
            }
            try
            {
                this->imgEdit->save();
            }
            catch(const std::invalid_argument& e)
            {
                setAndNotify(e.what());
                break;
            }
            catch(const std::runtime_error& e)
            {
                setAndNotify(e.what());
                break;
            }
            
            setAndNotify("File saved");
            break;
        }

        case CROP:
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before cropping");
                break;
            }
            if(arguments.size() < 5)
            {
                setAndNotify("Invalid use. Correct: crop (leftcorner Y value) (leftcorner X value) (rightcorner Y value) (rightcorner X value)"); 
                break;
            }
            this->imgEdit->crop(arguments[1], arguments[2], arguments[3], arguments[4]);
            setAndNotify("cropped");
            break;
        }

        case DITHER:
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before dithering");
                break;
            }
            if(arguments.size() < 2)
            {
                setAndNotify("Invalid use. Correct: dither nameOfDitheringAlg"); 
                break;
            }
            try
            {
                toUpper(arguments[1]);
                this->imgEdit->dither(arguments[1]);
            }
            catch(const std::bad_alloc& e)
            {
                setAndNotify(e.what());
                break;
            }
            catch(const std::invalid_argument& e)
            {
                setAndNotify(e.what());
                break;
            }
            
            setAndNotify("Dithering complete");
            break;
        }

        case SCALE:
        {
            if(!imgEdit)
            {
                setAndNotify("No file is loaded. Open one first before scaling");
                break;
            }
            if(arguments.size() < 3)
            {
                setAndNotify("Invalid use. Correct: resize newWidth newHeight"); 
                break;
            }
            imgEdit->scale(arguments[1], arguments[2]);
            setAndNotify("Scaling complete");
            break;
        }

        case EXIT: //----------------------------------------------------------------------------------------------
        {
            if(imgEdit)
            {
                if(imgEdit->getLoadState())
                {
                    setAndNotify("A file is loaded. Close it first before exiting");
                    break;
                }
            }
            
            setAndNotify("Exiting...");
            break;
        }
        
        case INVALID_COMMAND: //----------------------------------------------------------------------------------------------
        {
            std::string str = "invalid command!";
            this->setMessageSbj(str);
            this->Notify();
            break;
        }

        default: //----------------------------------------------------------------------------------------------
        {
            std::string str = "Unknown error :(";
            this->setMessageSbj(str);
            this->Notify();
            break;
        }
    }
}

const CommandType Controller::stringToCommand(const std::string &command) 
{
    if((strcmp(command.c_str(), "OPEN") == 0))
        return CommandType::OPEN;

    if((strcmp(command.c_str(), "CLOSE") == 0))
        return CommandType::CLOSE;

    if((strcmp(command.c_str(), "SAVEAS") == 0))
        return CommandType::SAVEAS;

    if((strcmp(command.c_str(), "NEW") == 0))
        return CommandType::NEW;

    if((strcmp(command.c_str(), "SAVE") == 0))
        return CommandType::SAVE;
    
    if((strcmp(command.c_str(), "EXIT") == 0))
        return CommandType::EXIT;

    if((strcmp(command.c_str(), "CROP") == 0))
        return CommandType::CROP;

    if((strcmp(command.c_str(), "SCALE") == 0))
        return CommandType::SCALE;

    if((strcmp(command.c_str(), "DITHER") == 0))
        return CommandType::DITHER;
    else
        return CommandType::INVALID_COMMAND;
}

Controller::Controller(Subject &subject) : Observer(subject)
{
    this->imgEdit = nullptr;
}

Controller::Controller() : Observer(), Subject()
{
    this->imgEdit = nullptr;
}

Controller::~Controller() 
{
    delete this->imgEdit;
}

const void Controller::toUpper(std::string& command) 
{
    std::size_t size = command.size();
    for(std::size_t i = 0; i < size; i++)
    {
        command[i] = toupper(command[i]); 
    }
}

std::vector<std::string> Controller::proccessInput(const std::string& input) 
{
    std::vector<std::string> subStr;

    std::size_t inputLen = input.length();
    std::size_t prevoiusPos = 0;
    std::size_t count = 0;
    for(std::size_t i = 0; i <= inputLen; i++)
    {
        //Handles arguments with quotes("")
        if(input[i] == '"')
        {
            std::size_t countQuote = 0;
            std::size_t startPos = i + 1;
            do
            {
                countQuote++;
                i++;
            } while (input[i] != '"');

            subStr.push_back(input.substr(startPos, countQuote - 1));
            return subStr;
        }

        //Handles normal input seprarated by spaces
        count++;
        if(input[i] == ' ' || input[i] == '\0')
        {
            subStr.push_back(input.substr(prevoiusPos, count - 1));

            prevoiusPos = i + 1;
            count = 0; 
        }
    }

    return subStr;
}

void Controller::setAndNotify(std::string message) 
{
    this->setMessageSbj(message);
    this->Notify();
}

void Controller::deleteImgEditor() 
{
    if(imgEdit != nullptr)
    {
        delete this->imgEdit;
        this->imgEdit = nullptr;
    }
}

void Controller::checkIfCorrectFormatUsed(const std::string& filepath) 
{
    auto pos = filepath.find_last_of('.');
    std::string format = filepath.substr(pos+1);

    switch (imgEdit->getFileFormat())
    {
    case P1:
    {
        if(format != "pbm")
            throw std::invalid_argument("Can`t save pbm as other format. Use (.pbm)");
        break;
    }
    case P2:
    {
        if(format != "pgm")
            throw std::invalid_argument("Can`t save pgm as other format. Use (.pgm)");
        break;
    }
    case P3:
    {
        if(format != "ppm")
            throw std::invalid_argument("Can`t save ppm as other format. Use (.ppm)");
        break;
    }
    
    default:
        throw std::invalid_argument("File format is unknown, can`t save");
        break;
    }
}