#pragma once
#include "..\observer-framework\observer.h" 
#include "..\observer-framework\subject.h"
#include "imageEditor.h"

#include <vector>
#include <cstring>
#include <iostream>

enum CommandType //Supported commands
{
    OPEN,
    CLOSE,
    NEW,
    SAVE,
    SAVEAS,
    EXIT,
    CROP,
    DITHER,
    SCALE,
    INVALID_COMMAND
};

class Controller : public Observer, public Subject
{
    imageEditor* imgEdit;

    /*
        Converts string to CommandType
        @param command string to be converted
    */
    const CommandType stringToCommand(const std::string& command);

    /*
        Converts string to uppercase
    */
    const void toUpper(std::string& command);

    /*
        Divides the input string to substrings
        @return vector with substrings
    */
    std::vector<std::string> proccessInput(const std::string& input);

    //Combines the two subject`s methods into one
    void setAndNotify(std::string message);

    void deleteImgEditor();

    void checkIfCorrectFormatUsed(const std::string& filepath);

public:

    /*
        Attaches observer to subject
        @param subject sbj to be attached to
        @return ref to class Controller  
    */
    Controller(Subject &subject);
    Controller();
    ~Controller();

    /*
        Calls stringToCommand. Adds a switch statement to determine which method to call
    */
    virtual void Update(const std::string &message_from_subject_input) override;
};