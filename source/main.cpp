#include <iostream>
#include <cstring>
#include "controller.h"
#include "view.h"

int main()
{
    View& viewer = View::getInstance();
        
    Controller cont(viewer);
    cont.Attach(&viewer);

    bool state = true;
    do
    {
        std::cout << "Enter input: " << std::endl;
        std::string in;
        std::getline(std::cin, in);

        viewer.getInput(in);

        if(strcmp(viewer.getMessageObs().c_str() , "Exiting...") == 0)
        {
            state = false;
        }
        
    } while (state);

    return 0;
}