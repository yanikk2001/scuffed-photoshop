#include "imageEditor.h"
#include <iostream>

imageEditor::imageEditor(const std::string& filepath_input) : filepath(filepath_input)
{
    std::ifstream img(filepath);
    if(!img.is_open())
    {
        throw std::runtime_error("Failed to open image");
    }

    std::string fileFormat;
    img >> fileFormat;

    try
    {
        loadImg(fileFormat); //Fills fformat and file private proprties of the class 
    }
    catch(const std::invalid_argument& e)
    {
        img.close();
        throw e;
    }
    
    this->isLoaded = true;
    img.close();
}

imageEditor::imageEditor(const std::string& width, const std::string& height, const std::string& color) : filepath("0x0000000")
{
    try
    {
        newFileHelper(width, height, color);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }
    
    this->isLoaded = true;
}

imageEditor::~imageEditor() 
{
    this->isLoaded = false;
    this->filepath = "0x0000000";
}

void imageEditor::save() 
{
    if(this->filepath == "0x0000000")
    {
        throw std::invalid_argument("No save location selected. Use SAVEAS (filepath)");
    }
    try
    {
        saveHelper(this->filepath);
    }
    catch(const std::runtime_error& e)
    {
        throw e;
    }
    catch(const std::invalid_argument& e)
    {
        throw e;
    }
}

void imageEditor::saveAs(const std::string& saveLocation) 
{
    try
    {
        saveHelper(saveLocation);
    }
    catch(const std::runtime_error& e)
    {
        throw e;
    }
    catch(const std::invalid_argument& e)
    {
        throw e;
    }

    this->filepath = saveLocation;
    
}

bool imageEditor::close() 
{
    static unsigned count = 0;
    if(count == 1)
    {
        count = 0;
        return true;
    }
    if(this->filepath == "0x0000000")
    {
        count++;
        throw std::invalid_argument("This file is not saved. If you wish to close it anyway: type close again");
    }
    return true;
}

void imageEditor::crop(const std::string& cor1, const std::string& cor2, const std::string& cor3, const std::string& cor4) 
{
    recpoint leftcorner, rightcorner;
    leftcorner.y = atoi(cor1.c_str());
    leftcorner.x = atoi(cor2.c_str());
    rightcorner.y = atoi(cor3.c_str());
    rightcorner.x = atoi(cor4.c_str());

    switch (this->fformat)
    {
    case P1:
    {
        auto f = std::get<PBM>(this->file);
        std::vector<BIT> cropped = cropHelper<BIT, PBM>(leftcorner, rightcorner, f);
        unsigned int newHeight = abs(leftcorner.y - rightcorner.y) + 1;
        unsigned int newWidth = abs(leftcorner.x - rightcorner.x) + 1;
        f.copy(cropped, newWidth, newHeight);
        this->file = f;
        break;
    }
    case P2:
    {
        auto f = std::get<PGM>(this->file);
        std::vector<BW> cropped = cropHelper<BW, PGM>(leftcorner, rightcorner, f);
        unsigned int newHeight = abs(leftcorner.y - rightcorner.y) + 1;
        unsigned int newWidth = abs(leftcorner.x - rightcorner.x) + 1;
        f.copy(cropped, newWidth, newHeight);
        this->file = f;
        break;
    }
    case P3:
    {
        auto f = std::get<PPM>(this->file);
        std::vector<RGB> cropped = cropHelper<RGB, PPM>(leftcorner, rightcorner, f);
        unsigned int newHeight = abs(leftcorner.y - rightcorner.y) + 1;
        unsigned int newWidth = abs(leftcorner.x - rightcorner.x) + 1;
        f.copy(cropped, newWidth, newHeight);
        this->file = f;
        break;
    }
    
    default:
        break;
    }
}

template<typename T, typename FORMAT>
std::vector<T> imageEditor::cropHelper(recpoint& leftcorner, recpoint& rightcorner, FORMAT& format) 
{
    std::vector<T> resized;

    unsigned int width = format.getWidth();
    std::vector<T> pixels = format.getData();

    unsigned int numOfColumns = width;
    unsigned int newHeight, newWidth;
    
    newHeight = abs(leftcorner.y - rightcorner.y) + 1;
    newWidth = abs(leftcorner.x - rightcorner.x) + 1;

    format.calibratePoints(leftcorner);
    format.calibratePoints(rightcorner);

    for(auto i = leftcorner.y; i < int(newHeight) + leftcorner.y; i++)
    {
        for(auto j = leftcorner.x; j < int(newWidth) + leftcorner.x; j++)
        {
            resized.push_back((pixels[(i*numOfColumns + j)]));
        }
    }
    
    return resized;
}

void imageEditor::scale(const std::string& newWidth, const std::string& newHeight) 
{
    unsigned int newWid = atoi(newWidth.c_str());
    unsigned int newHt = atoi(newHeight.c_str());

    switch (this->fformat)
    {
    case P1:
    {
        auto f = std::get<PBM>(this->file);
        std::vector<BIT> newImg = resizePixels<BIT>(f.getData(), int(f.getWidth()), int(f.getHeight()), int(newWid), int(newHt));
        f.copy(newImg, newWid, newHt);
        this->file = f;
        break;
    }
    case P2:
    {
        auto f = std::get<PGM>(this->file);
        std::vector<BW> newImg = resizePixels<BW>(f.getData(), int(f.getWidth()), int(f.getHeight()), int(newWid), int(newHt));
        f.copy(newImg, newWid, newHt);
        this->file = f;
        break;
    }
    case P3:
    {
        auto f = std::get<PPM>(this->file);
        std::vector<RGB> newImg = resizePixels<RGB>(f.getData(), int(f.getWidth()), int(f.getHeight()), int(newWid), int(newHt));
        f.copy(newImg, newWid, newHt);
        this->file = f;
        break;
    }
    
    default:
        break;
    }
}

template <typename T>
std::vector<T> imageEditor::resizePixels(std::vector<T> pixels, int w1, int h1, int w2, int h2)
{
    std::vector<T> temp;
    double x_ratio = w1 / (double)w2;
    double y_ratio = h1 / (double)h2;
    double px, py;
    for (int i = 0; i<h2; i++)
    {
        for (int j = 0; j<w2; j++)
        {
            px = floor(j*x_ratio);
            py = floor(i*y_ratio);
            temp.push_back(pixels[(int)((py*w1) + px)]);
        }
    }
    return temp;
}

const fileformat imageEditor::getFileFormat() const
{
    return this->fformat;
}

const bool imageEditor::getLoadState() const
{
    return this->isLoaded;
}

/*
    ADD NEW CASES IN THE BELLOW FUNCTIONS TO PRIVIDE FUNCTIONALITY FOR OTHER IMG FORMATS
*/

void imageEditor::saveHelper(const std::string& saveLocation) 
{
    if(!isLoaded)
    {
        throw std::invalid_argument("No file loaded");
    }
    switch (this->fformat)
    {
    case P1:
    {
        try
        {
            auto f = std::get<PBM>(this->file);
            f.save(saveLocation);
        }
        catch(const std::runtime_error& e)
        {
            throw e;
        }
        
        break;
    }
    
    case P2:
    {
        try
        {
            auto f = std::get<PGM>(this->file);
            f.save(saveLocation);
        }
        catch(const std::runtime_error& e)
        {
            throw e;
        }
        break;
    }

    case P3:
    {
        try
        {
            auto f = std::get<PPM>(this->file);
            f.save(saveLocation);
        }
        catch(const std::runtime_error& e)
        {
            throw e;
        }
        
        break;
    }
    
    default:
        break;
    }
}

void imageEditor::newFileHelper(const std::string& width, const std::string& height, std::string color) 
{
    if(color[0] == '#')
    {
        color.erase(color.begin());
    }

    unsigned int length = color.length();
    if(length == 1)
    {
        PBM pbm;
        try
        {
            pbm.fill(width, height, color);
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }

        this->file = pbm;
        this->fformat = P1;
    }
    else if(length == 2)
    {
        PGM pgm;
        try
        {
            pgm.fill(width, height, color);
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }

        this->file = pgm;
        this->fformat = P2;
    }
    else if(length == 6)
    {
        PPM ppm;
        try
        {
            ppm.fill(width, height, color);
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }

        this->file = ppm;
        this->fformat = P3;
    }
    else
    {
        throw std::out_of_range(
            "Invalid color format. Right format: For PBM: #(value) where value is either 0 or 1, For PGM: #(hex value) where value is between 0 and 255, For PPM: #(hex value)(hex value)(hex value), where hex value is between 0 and 255"
        );
    }
}

void imageEditor::loadImg(const std::string& format) 
{
    if(format == "P1")
    {
        PBM pbm;
        try
        {
            pbm.open(this->filepath);
        }
        catch(const std::invalid_argument& e)
        {
            throw e;
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }
    
        this->fformat = P1;
        this->file = pbm;
    }
    else if(format == "P2")
    {
        PGM pgm;
        try
        {
            pgm.open(this->filepath);
        }
        catch(const std::invalid_argument& e)
        {
            throw e;
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }
    
        this->fformat = P2;
        this->file = pgm;
    }
    else if(format == "P3")
    {
        PPM ppm;
        try
        {
            ppm.open(this->filepath);
        }
        catch(const std::invalid_argument& e)
        {
            throw e;
        }
        catch(const std::out_of_range& e)
        {
            throw e;
        }
    
        this->fformat = P3;
        this->file = ppm;
    }
    else
    {
        throw std::invalid_argument("Invalid file format");
    }
}

/*
    DITHERING OPTIONS --------------------------------------------------
*/
void imageEditor::dither(const std::string& ditherName) 
{
    switch (this->fformat)
    {
    case P1:
        throw std::invalid_argument("No point dithering a pbm file");
        break;

    case P2:
    {
       auto f = std::get<PGM>(this->file);
        try
        {
            unsigned int width = f.getWidth();
            unsigned int height = f.getHeight();

            Dither dith(f.getData(), width, height);
            dith.applyDither(ditherName);

            PBM pbm;
            pbm.copy(dith.getData(), width, height);
            this->file = pbm;
            this->fformat = P1;
            updateFilepath();
        }
        catch(const std::bad_alloc& e)
        {
            throw e;
        }
        catch(const std::invalid_argument& e)
        {
            throw e;
        }
        
        break;
    }

    case P3:
    {
        auto f = std::get<PPM>(this->file);
        try
        {
            unsigned int width = f.getWidth();
            unsigned int height = f.getHeight();

            Dither dith(f.getData(), width, height);
            dith.applyDither(ditherName);

            PBM pbm;
            pbm.copy(dith.getData(), width, height);
            this->file = pbm;
            this->fformat = P1;
            updateFilepath();
        }
        catch(const std::bad_alloc& e)
        {
            throw e;
        }
        catch(const std::invalid_argument& e)
        {
            throw e;
        }
        
        break;
    }
    
    default:
        throw std::invalid_argument("Invalid file format");
        break;
    }
}

void imageEditor::updateFilepath()
{
    if(filepath != "0x0000000")
    {
        auto pos = filepath.find_last_of('.');
        filepath.replace(pos, filepath.length() - 1, ".pbm");
    }
}

