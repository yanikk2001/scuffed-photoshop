#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

struct recpoint
{
    int x;
    int y;
};

class I_Format
{
protected:

    unsigned int width, height;


    unsigned int hexToDec(const std::string& hex);

    virtual void checkValidity(const std::string& str) = 0;

public:
    virtual ~I_Format(){};

    /*
        Creates ppm file filled with one color
        @param color_input only hex input allowed
    */
    virtual void fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) = 0;

    /* 
        Opens ppm file and loads it into memory
        @param path ppm file location
        @exception invalid_argument if format is invalid or cant open file
    */
    virtual void open(const std::string& path) = 0;

    /* 
        Saves to a file
        @param path file location
    */
    virtual void save(const std::string& path) = 0;

    //If the user input is out of range it increases/decreases it to be within possible coordinates
    void calibratePoints(recpoint& corner);

    const unsigned int getWidth() const;
    const unsigned int getHeight() const;
};