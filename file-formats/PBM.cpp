#include "PBM.h"

PBM::PBM()
{
    width = 0;
    height = 0;
}

void PBM::fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) 
{
    width = atoi(width_input.c_str());
    height = atoi(height_input.c_str());

    BIT color;
    try
    {
        color = processColor(color_input);
    }
    catch(const std::out_of_range& e)
    {
        width = 0;
        height = 0;
        throw e;
    }
    
    for(std::size_t i = 0; i < height * width; i++)
    {
        this->pixels.push_back(color);
    }

}

void PBM::open(const std::string& path)
{
    std::ifstream fp(path.c_str());
    if (fp.fail() || !fp.is_open())
        throw std::invalid_argument("Can`t open PBM file");

    //Read the Magic Number
    std::string mg_num, width_str, height_str;
    fp >> mg_num;

    if (mg_num != "P1")
    {
        fp.close();
        throw std::invalid_argument("The file is not a ASCII PBM file!");
    }

    fp >> width_str >> height_str;
    width  = atoi(width_str .c_str()); //Takes the number std::string and converts it to an integer
    height = atoi(height_str.c_str());
    
    //Obliterate the vector
    pixels.clear();

    //Read the values into the vector directly.
    BIT onebit;
    std::string bit;
    for (std::size_t i = 0; i < width * height; i++)
    {
        fp >> bit;

        try
        {
            checkValidity(bit);
        }
        catch(const std::out_of_range& e)
        {
            width = 0;
            height = 0;
            throw e;
        }
        
        onebit.value = atoi(bit.c_str());

        pixels.push_back(onebit);
    }

    fp.close();
}

void PBM::save(const std::string& path) 
{
    std::ofstream fp(path, std::ios::trunc);
    if(fp.fail() || !fp.is_open())
    {
        throw std::runtime_error("Can`t save PBM file");
    }

    fp << "P1" << '\n';
    fp << std::to_string(this->width) << " " << std::to_string(this->height) << '\n';

    std::size_t j = 0;
    for(std::size_t i = 0; i < width * height; i++)
    {   
        j++;
        fp << std::to_string(pixels[i].value) << " ";
        if(j == width)
        {
            fp << '\n';
            j = 0;
        }
    }
    
    fp.close();
}

void PBM::copy(std::vector<BIT> other, unsigned int& width_input, unsigned int& height_input) 
{
    this->pixels = other;
    this->width = width_input;
    this->height = height_input;
}

std::vector<BIT> PBM::getData() const
{
    return this->pixels;
}

BIT PBM::processColor(const std::string& color)
{
    unsigned int prcolor;
    try
    {
        prcolor = hexToDec(color); 
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    BIT x;
    x.value = prcolor;
    return x;
}

void PBM::checkValidity(const std::string& str)
{
    unsigned int x = atoi(str.c_str());
    if(x != 1 && x != 0)
        throw std::out_of_range("Corrupt pbm file containing invalid data");
}


