#include "PGM.h"

PGM::PGM()
{
    width = 0;
    height = 0;
    colorRange = 0;
}

void PGM::fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) 
{
    unsigned int color;
    try
    {
        color = hexToDec(color_input);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }
    
    width = atoi(width_input.c_str());
    height = atoi(height_input.c_str());
    colorRange = 255;

    BW tmp = color;
    for(std::size_t i = 0; i < height * width; i++)
    {
        this->pixels.push_back(tmp);
    }
}

void PGM::open(const std::string& path)
{
    std::ifstream fp(path.c_str());
    if (fp.fail() || !fp.is_open())
        throw std::invalid_argument("Can`t open PGM file");

    //Read the Magic Number
    std::string mg_num, width_str, height_str, range_str;
    fp >> mg_num;

    if (mg_num != "P2")
    {
        fp.close();
        throw std::invalid_argument("The file is not a ASCII PGM file!");
    }

    fp >> width_str >> height_str >> range_str;
    width  = atoi(width_str .c_str()); //Takes the number std::string and converts it to an integer
    height = atoi(height_str.c_str());
    colorRange = atoi(range_str.c_str());

    //Obliterate the vector
    pixels.clear();

    //Read the values into the vector directly.
    BW tmp;
    std::string pixel;
    for (std::size_t i = 0; i < width * height; i++)
    {
        fp >> pixel;

        try
        {
            checkValidity(pixel);
        }
        catch(const std::out_of_range& e)
        {
            width = 0;
            height = 0;
            colorRange = 0;
            throw e;
        }
        
        tmp = atoi(pixel.c_str());

        pixels.push_back(tmp);
    }

    fp.close();
}

void PGM::save(const std::string& path) 
{
    std::ofstream fp(path, std::ios::trunc);
    if(fp.fail() || !fp.is_open())
    {
        throw std::runtime_error("Can`t save PGM file");
    }

    fp << "P2" << '\n';
    fp << std::to_string(this->width) << " " << std::to_string(this->height) << '\n';
    fp << std::to_string(this->colorRange) << '\n';

    std::size_t j = 0;
    for(std::size_t i = 0; i < width * height; i++)
    {
        j++;
        fp << std::to_string(pixels[i]) << " ";
        if(j == width)
        {
            j = 0;
            fp << '\n';
        }
    }
    
    fp.close();
}

std::vector<BW> PGM::getData() const
{
    return this->pixels;
}

void PGM::checkValidity(const std::string& str) 
{
    unsigned int x = atoi(str.c_str());

    if(x < 0 && x > 255)
        throw std::out_of_range("Corrupt PGM file containing invalid data");
}

void PGM::copy(std::vector<BW> other, unsigned int& width_input, unsigned int& height_input) 
{
    this->pixels = other;
    this->width = width_input;
    this->height = height_input;
}


