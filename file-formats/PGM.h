#pragma once
#include "InterfaceFormats.h"

typedef unsigned int BW; //Black & White and all in between

class PGM : public I_Format
{
    private:
        unsigned int colorRange;
        std::vector<BW> pixels; //

        unsigned int processColor(const std::string& color);
        virtual void checkValidity(const std::string& str) override; 

    public:
        PGM();
        virtual ~PGM(){};

        virtual void fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) override;

        virtual void open(const std::string& path) override;

        virtual void save(const std::string& path) override;

        void copy(std::vector<BW> other, unsigned int& width_input, unsigned int& height_input);

        //Getters
        std::vector<BW> getData() const; //
};