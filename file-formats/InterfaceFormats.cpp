#include "InterfaceFormats.h"

unsigned int I_Format::hexToDec(const std::string& hex) 
{
    unsigned int decimal;
    std::stringstream stream;
    stream << hex;
    stream >> std::hex >> decimal;

    if(decimal < 0 && decimal > 255)
    {
        throw std::out_of_range("Invalid hex color value. Value should be between 0 and 255");
    }

    return decimal; 
}

void I_Format::calibratePoints(recpoint& corner) 
{
    if(corner.x >= int(width))
        corner.x = width - 1;
    if(corner.y >= int(height))
        corner.y = height - 1;
    if(corner.x < 0)
        corner.x = 0;
    if(corner.y < 0)
        corner.y = 0;
}

const unsigned int I_Format::getWidth() const
{
    return this->width;
}

const unsigned int I_Format::getHeight() const
{
    return this->height;
} 