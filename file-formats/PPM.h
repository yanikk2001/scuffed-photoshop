#pragma once
#include "InterfaceFormats.h"

#include "RGB.h"

class PPM : public I_Format
{
    private:
        unsigned int colorRange;
        std::vector<RGB> pixels; //

        std::vector<std::string> processColor(const std::string& color);
        virtual void checkValidity(const std::string& str) override; 

    public:
        PPM();
        virtual ~PPM(){};

        /*
            Creates ppm file filled with one color
            @param color_input only hex input allowed
        */
        virtual void fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) override;

        /* 
            Opens ppm file and loads it into memory
            @param path ppm file location
            @exception invalid_argument if format is invalid or cant open file
        */
        virtual void open(const std::string& path) override;

        /* 
            Saves to a file
            @param path file location
        */
        virtual void save(const std::string& path) override;

        void copy(std::vector<RGB> other, unsigned int& width_input, unsigned int& height_input);

        friend std::ofstream& operator<<(std::ofstream& fout, std::string& str);

        //Getters
        std::vector<RGB> getData() const; //
};