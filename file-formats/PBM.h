#pragma once
#include "InterfaceFormats.h"
#include "OneBit.h"

class PBM : public I_Format
{
    std::vector<BIT> pixels;
    BIT processColor(const std::string& color);

    virtual void checkValidity(const std::string& str) override; 
public:
        PBM();
        virtual ~PBM(){};

        virtual void fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) override;

        virtual void open(const std::string& path) override;

        virtual void save(const std::string& path) override;

        //Sets other vector, width and height as own params
        void copy(std::vector<BIT> other, unsigned int& width_input, unsigned int& height_input);

        //Getters
        std::vector<BIT> getData() const; 
};