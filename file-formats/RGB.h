#pragma once

struct RGB
{
    //Constructor to set R, G, and B to 0 automatically.
    RGB() : R(0), G(0), B(0) {};

    //Values
    unsigned char R, G, B;
};