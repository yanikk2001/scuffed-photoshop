#include "PPM.h"

PPM::PPM()
{
    width = 0;
    height = 0;
    colorRange = 0;
}

void PPM::fill(const std::string& width_input, const std::string& height_input, const std::string& color_input) 
{
    std::vector<std::string> color;
    color = processColor(color_input);

    width = atoi(width_input.c_str());
    height = atoi(height_input.c_str());
    colorRange = 255;

    try
    {
        RGB tmp;
        for(std::size_t i = 0; i < height * width; i++)
        {
            tmp.R = hexToDec(color[0]);
            tmp.G = hexToDec(color[1]);
            tmp.B = hexToDec(color[2]);

            this->pixels.push_back(tmp);
        }
    }
    catch(const std::out_of_range& e)
    {
        width = 0;
        height = 0;
        colorRange = 0;
        pixels.clear();
        throw e;
    }
}

void PPM::open(const std::string& path)
{
    std::ifstream fp(path.c_str());
    if (fp.fail() || !fp.is_open())
        throw std::invalid_argument("Can`t open ppm file");

    //Read the Magic Number
    std::string mg_num, width_str, height_str, range_str;
    fp >> mg_num;

    if (mg_num != "P3")
    {
        fp.close();
        throw std::invalid_argument("The file is not a ASCII PPM file!");
    }

    fp >> width_str >> height_str >> range_str;
    width  = atoi(width_str .c_str()); //Takes the number std::string and converts it to an integer
    height = atoi(height_str.c_str());
    colorRange = atoi(range_str.c_str());

    //Obliterate the vector
    pixels.clear();

    //Read the values into the vector directly.
    RGB tmp;
    std::string _R, _G, _B;
    for (std::size_t i = 0; i < width * height; i++)
    {
        fp >> _R >> _G >> _B;

        try
        {
            checkValidity(_R);
            checkValidity(_G);
            checkValidity(_B);
        }
        catch(const std::out_of_range& e)
        {
            width = 0;
            height = 0;
            colorRange = 0;
            throw e;
        }
        
        tmp.R = atoi(_R.c_str());
        tmp.G = atoi(_G.c_str());
        tmp.B = atoi(_B.c_str());

        pixels.push_back(tmp);
    }

    fp.close();
}

void PPM::save(const std::string& path) 
{
    std::ofstream fp(path, std::ios::trunc);
    if(fp.fail() || !fp.is_open())
    {
        throw std::runtime_error("Can`t save ppm file");
    }

    fp << "P3" << '\n';
    fp << std::to_string(this->width) << " " << std::to_string(this->height) << '\n';
    fp << std::to_string(this->colorRange) << '\n';

    std::size_t j = 0;
    for(std::size_t i = 0; i < width * height; i++)
    {
        j++;
        fp << std::to_string(pixels[i].R) << " " << std::to_string(pixels[i].G) << " " << std::to_string(pixels[i].B) << "   ";
        if(j == width)
        {
            j = 0;
            fp << '\n';
        }
    }
    
    fp.close();
}

std::ofstream& operator<<(std::ofstream& fout, std::string& str)
{
    fout << str << " ";
    return fout;
}

std::vector<RGB> PPM::getData() const
{
    return this->pixels;
}

std::vector<std::string> PPM::processColor(const std::string& color) 
{
    std::vector<std::string> vec;
    vec.push_back(color.substr(0,2));
    vec.push_back(color.substr(2,2));
    vec.push_back(color.substr(4,2));

    return vec;
}

void PPM::checkValidity(const std::string& str) 
{
    unsigned int x = atoi(str.c_str());

    if(x < 0 && x > 255)
        throw std::out_of_range("Corrupt ppm file containing invalid data");
}

void PPM::copy(std::vector<RGB> other, unsigned int& width_input, unsigned int& height_input) 
{
    this->pixels = other;
    this->width = width_input;
    this->height = height_input;
}

