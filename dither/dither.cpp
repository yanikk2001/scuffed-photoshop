#include "dither.h"
#include <iostream>

Dither::Dither(const std::vector<BW>& data, const unsigned int& width_input, const unsigned int& height_input) : width(width_input), height(height_input)
{
    try
    {
        pixels = new unsigned int*[height];
        for(unsigned int i = 0; i < height; ++i)
            pixels[i] = new unsigned int[width];
    }
    catch(const std::bad_alloc& e)
    {
        throw e;
    }

    for(unsigned int i = 0; i < height; i++)
    {
        for(unsigned int j = 0; j < width; j++)
        {
            pixels[i][j] = (data[(i*width + j)]);
        }
    }
}

Dither::Dither(const std::vector<RGB>& data, const unsigned int& width_input, const unsigned int& height_input) : width(width_input), height(height_input)
{
    try
    {
        pixels = new unsigned int*[height];
        for(unsigned int i = 0; i < height; ++i)
            pixels[i] = new unsigned int[width];
    }
    catch(const std::bad_alloc& e)
    {
        throw e;
    }

    for(unsigned int i = 0; i < height; i++)
    {
        for(unsigned int j = 0; j < width; j++)
        {
            RGB pixel = (data[(i*width + j)]);
            unsigned int average = (pixel.R + pixel.G + pixel.B) / 3;
            
            pixels[i][j] = average;
        }
    }
}

std::vector<idk> Dither::initialise(std::vector<int>& pos1, std::vector<int>& pos2, std::vector<unsigned int>& values)
{
    std::vector<idk> vec;
    idk a;
    unsigned int size = pos1.size();
    for(unsigned int i = 0; i < size; i++)
    {
        a.pos1 = pos1[i];
        a.pos2 = pos2[i];
        a.value = values[i];

        vec.push_back(a);
    }
    return vec;
}

void Dither::applyDither(const std::string& ditherName) 
{
    if(ditherName == "FLOYD-STEINBERG")
    {
        std::vector<int> pos1 =             {0, 1, 1, 1};
        std::vector<int> pos2 =             {1, -1, 0, 1};
        std::vector<unsigned int> values =  {7, 3, 5, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(16, algorithmPattern);
        
        return;
    }
    if(ditherName == "J-J-N")
    {
        std::vector<int> pos1 =             {0, 0,  1,  1, 1, 1, 1,  2,  2, 2, 2, 2};
        std::vector<int> pos2 =             {1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2};
        std::vector<unsigned int> values =  {7, 5,  3,  5, 7, 5, 3,  1,  3, 5, 3, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(48, algorithmPattern);
        
        return;
    }
    if(ditherName == "STUCKI")
    {
        std::vector<int> pos1 =             {0, 0,  1,  1, 1, 1, 1,  2,  2, 2, 2, 2};
        std::vector<int> pos2 =             {1, 2, -2, -1, 0, 1, 2, -2, -1, 0, 1, 2};
        std::vector<unsigned int> values =  {8, 4,  2,  4, 8, 4, 2,  1,  2, 4, 2, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(42, algorithmPattern);
        
        return;
    }
    if(ditherName == "ATKINSON")
    {
        std::vector<int> pos1 =             {0, 0,  1, 1, 1, 2};
        std::vector<int> pos2 =             {1, 2, -1, 0, 1, 0};
        std::vector<unsigned int> values =  {1, 1,  1, 1, 1, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(8, algorithmPattern);
        
        return;
    }
    if(ditherName == "BURKES")
    {
        std::vector<int> pos1 =             {0, 0,  1,  1, 1, 1, 1};
        std::vector<int> pos2 =             {1, 2, -2, -1, 0, 1, 2};
        std::vector<unsigned int> values =  {8, 4,  2,  4, 8, 4, 2};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(32, algorithmPattern);
        
        return;
    }
    if(ditherName == "SIERRA")
    {
        std::vector<int> pos1 =             {0, 0,  1,  1, 1, 1, 1,  2, 2, 2};
        std::vector<int> pos2 =             {1, 2, -2, -1, 0, 1, 2, -1, 0, 1};
        std::vector<unsigned int> values =  {5, 3,  2,  4, 5, 4, 2,  2, 3, 2};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(32, algorithmPattern);
        
        return;
    }
    if(ditherName == "TWO-ROW-SIERRA")
    {
        std::vector<int> pos1 =             {0, 0,  1,  1, 1, 1, 1};
        std::vector<int> pos2 =             {1, 2, -2, -1, 0, 1, 2};
        std::vector<unsigned int> values =  {4, 3,  1,  2, 3, 2, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(16, algorithmPattern);
        
        return;
    }
    if(ditherName == "SIERRA-LITE")
    {
        std::vector<int> pos1 =             {0,  1, 1};
        std::vector<int> pos2 =             {1, -1, 0};
        std::vector<unsigned int> values =  {2,  1, 1};
        
        std::vector<idk>algorithmPattern = initialise(pos1, pos2, values);

        combine(4, algorithmPattern);
        
        return;
    }

    throw std::invalid_argument("Invalid dither name");
}

Dither::~Dither() 
{
    for(unsigned int i = 0; i < height; i++)
    {
        delete[] this->pixels[i];
    }
    delete[] pixels;
}

std::vector<BIT> Dither::getData() 
{
    std::vector<BIT> vec;

    for(unsigned int i = 0; i < height; i++)
    {
        for(unsigned int j = 0; j < width; j++)
        {
            BIT bit;
            bit.value = pixels[i][j];
            vec.push_back(bit);
        }
    }

    return vec;
}

void Dither::combine(int del, std::vector<idk> alg)
{
    for(unsigned int i = 0; i < height; i++)
    {
        for(unsigned int j = 0; j < width; j++)
        {
            int error = turnToWhiteOrBlack(i, j);
            error /= del;

            for(unsigned int k = 0; k < alg.size(); k++)
            {
                unsigned int pos1 = i + alg[k].pos1;
                unsigned int pos2 = j + alg[k].pos2;

                if(pos1 < 0 || pos2 < 0)
                    continue;
                if(pos1 >= height || pos2 >= width)
                    continue;

                pixels[pos1][pos2] += alg[k].value * error;
            }
            
        }
    }
}

int Dither::turnToWhiteOrBlack(unsigned int& i, unsigned int& j)
{
    if(pixels[i][j] > 255)
        pixels[i][j] = 255;
    int rangeToBlack = pixels[i][j];
    int rangeToWhite = 255 - pixels[i][j];
 
    if(rangeToBlack < rangeToWhite)
    {
        pixels[i][j] = 0;
        return rangeToBlack;
    }
    else if(rangeToWhite <= rangeToBlack)
    {
        pixels[i][j] = 1;
        return -rangeToWhite;
    }
}

