#pragma once
#include <vector>
#include <string>

#include "..\file-formats\RGB.h"
#include "..\file-formats\OneBit.h"
typedef unsigned int BW; //Black & White and all in between

//Couldn`t think of a name. Contains position(x,y or in other words width,height) against some pixel and value at this pos
struct idk 
{
    int pos1;
    int pos2;
    unsigned int value;
};

class Dither
{
    unsigned int** pixels = nullptr;
    
    const unsigned int height;
    const unsigned int width;

    /*
        At pos [i][j] checks if value is closer to 0(black) or 255(white) and sets it to the closest
        @return margin of error to be distributed among other pixels around
    */
    int turnToWhiteOrBlack(unsigned int& i, unsigned int& j);

    /*
        Merges the 3 vectors into one, pos1 holds x vals , pos2 - y, so that pixels[curr pos + x][curr pos + y] += value * error
    */
    std::vector<idk> initialise(std::vector<int>& pos1, std::vector<int>& pos2, std::vector<unsigned int>& values);

public:
    //Constructor when loading ppm images
    Dither(const std::vector<RGB>& data, const unsigned int& width_input, const unsigned int& height_input);
    //Constructor when loading pgm images
    Dither(const std::vector<BW>& data, const unsigned int& width_input, const unsigned int& height_input);


    /*
        Based on the alg name runs it
    */
    void applyDither(const std::string& ditherName);

    /*
        This function combines all dithering algorithms
        @arg del error division value
        @arg alg vector containing all info about how to propagate the error
    */
    void combine(int del, std::vector<idk> alg);

    ~Dither();

    /*
        Converts the 2d array into bit vector
    */
    std::vector<BIT> getData(); 
};